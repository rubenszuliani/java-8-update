package com.algaworks.dao;

import com.algaworks.modelo.Fatura;

import java.util.ArrayList;
import java.util.List;

public class FaturaDAO {

    public List<Fatura> buscarFaturasVencidas(){


        /**
         * Obviamente buscariamos isso no banco de dados
         */
        List<Fatura> faturas = new ArrayList();
        faturas.add(new Fatura("joao@joao.com", 350.00));
        faturas.add(new Fatura("maicon@joao.com", 200.00));
        faturas.add(new Fatura("carlos@joao.com", 400.00));

        return faturas;

    }



}
