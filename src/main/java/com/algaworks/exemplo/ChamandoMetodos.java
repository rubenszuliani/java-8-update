package com.algaworks.exemplo;

import com.algaworks.dao.FaturaDAO;
import com.algaworks.modelo.Fatura;

import java.util.List;

public class ChamandoMetodos {
    public static void main(String[] args) {
        List<Fatura> faturasVencidas = new FaturaDAO().buscarFaturasVencidas();

        //forma tradicional
        /*for (Fatura f: faturasVencidas){
            f.atualizarStatus();
        }*/

        //faturasVencidas.forEach(f-> f.atualizarStatus());

        /**
         * Referencias de Metodos
         * Para cada Fatura chama o Metodo atualizar Status
         */
        faturasVencidas.forEach(Fatura::atualizarStatus);
    }
}
