package com.algaworks.exemplo;

import com.algaworks.dao.FaturaDAO;
import com.algaworks.modelo.Fatura;
import com.algaworks.util.EnviadorEmail;

import java.util.List;

public class ExemploLambda {

    public static void main(String[] args) {
        List<Fatura> faturasVencidas = new FaturaDAO().buscarFaturasVencidas();

        EnviadorEmail enviadorEmail = new EnviadorEmail();

        //fazendo com For normal
        /*for (Fatura f : faturasVencidas){
            enviadorEmail.enviar(f.getEmailDevedor(), f.resumo());
        }*/
        //Expressoes Lambda
        faturasVencidas.forEach(f-> {
            enviadorEmail.enviar(f.getEmailDevedor(), f.resumo());
            f.setEmailEnviado(true);
        });




    }
}
