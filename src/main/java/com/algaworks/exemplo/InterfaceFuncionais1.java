package com.algaworks.exemplo;

import com.algaworks.impressao.compra.Compra;
import com.algaworks.impressao.impressora;
import com.algaworks.impressao.impressoras.impressoraHP;

/**
 * INTERFACE FUNCIONAL E QUANDO POSSUI APENAS UM METODO, POS AI CONSEGUIMOS CRIAR EXPRESSOES LAMBDAS ENCIMA DESSA
 * INTERFACE
 */
public class InterfaceFuncionais1 {
    public static void main(String[] args) {
       // impressora i = new impressoraHP();

        /**
         * = () - Quantos argumentos o metodo Imprimir da interface recebe ? no nosso caso é 0 entao nao colocaos nada
         *
         * Posso ter varias linhas chamando os metodos
         *
         * c Ja sabe que seria um Objeto do tipo Compra
         *
         * Nesse caso o impressora i = (c) e como se foce a classe impressoraHP
         */

        impressora i = (c) ->{
            System.out.println("Simulando a Impressao" + c);
        };

        /**
         *
         * Como a Interface so possui um metodo, sabemo que e uma inteface funcional, e podemos chamar essa interface
         * diretamente em qualquer classe, sem a necessidade de criar uma nova classe e fazer o implements
         *
         * Lembrando que é só apenas quando tiver " 1 " Metodo
         *
         */
        //i.imprimir();

        //criamos o Objeto
        Compra compra = new Compra("Sabonete", 1.5);

        //mandamos imprimir os dados direto da Interface, o compra ja retorna o ToString
        i.imprimir(compra);

    }

}
