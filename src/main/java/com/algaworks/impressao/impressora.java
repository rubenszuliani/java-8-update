package com.algaworks.impressao;

import com.algaworks.impressao.compra.Compra;

/**
 * Anotação que diz que e uma interface funcional
 * isso siguinifica que nao podemos adicionar outros metodos, pos teremos usado o lambda para frente
 * Sempre colocar essa anotação para que os programadores saibam que é uma interface funcional
 */
@FunctionalInterface
public interface impressora {

    public void imprimir(Compra c);

}
