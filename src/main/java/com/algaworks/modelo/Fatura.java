package com.algaworks.modelo;

public class Fatura {

    private String emailDevedor;
    private Double valor;
    private Boolean emailEnviado;

    public Boolean getEmailEnviado() {
        return emailEnviado;
    }

    public void setEmailEnviado(Boolean emailEnviado) {
        this.emailEnviado = emailEnviado;
    }

    public Fatura(String emailDevedor, Double valor) {
        this.emailDevedor = emailDevedor;
        this.valor = valor;
    }

    public String getEmailDevedor() {
        return emailDevedor;
    }

    public void setEmailDevedor(String emailDevedor) {
        this.emailDevedor = emailDevedor;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String resumo(){
        return "Valor devido: " + this.valor;
    }

    public void atualizarStatus(){
        System.out.println("Atuaizando status da fatura de Valor" + this.valor);
    }
}
