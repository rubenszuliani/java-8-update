package com.stream.modelo;

public class Fatura  {

    private String email;
    private Double valor;

    public Fatura(String email, Double valor) {
        this.email = email;
        this.valor = valor;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public boolean estaEmrisco(){
        return valor >= 250 ? true : false;
    }

    @Override
    public String toString() {
        return "Email: " + email + " Valor: " + valor;
    }


}
