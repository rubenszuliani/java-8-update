package com.stream.modelo;

import com.stream.modelo.Fatura;

import java.util.ArrayList;
import java.util.List;

public class FaturaDAO {

    public List<Fatura> buscaTodasFaturas() {
        List<Fatura> faturas = new ArrayList();
        faturas.add(new Fatura("joao@joao.com", 150.00));
        faturas.add(new Fatura("maicon@joao.com", 200.00));
        faturas.add(new Fatura("henrique@joao.com", 400.00));

        return faturas;
    }
}
