package com.stream.modelo;

import com.stream.modelo.Fatura;

import java.util.List;


/**
 * Numca colocar o nome da classe de Stream  kkkkkk
 * Stream e do Java 8 para trabalharmos com Listas e conseguir com expressoes lambdas criar uma
 */
public class Stream {

    public static void main(String[] args) {

        //buscar todas as faturas com valor maior que 250

        List<Fatura> faturas = new FaturaDAO().buscaTodasFaturas();

        //Fazendo a Logica sem Stream
        /*for (Fatura f: faturas){
            if(f.getValor() > 250){
                System.out.println("Fatura acima de 250: " + f);
            }
        }*/

        /**
         * Fazendo com Stream e reference Metode
         */

        /**
         * Temos um metodo que retorna um stream de fatura
         * Tem varios metodos que vai nos nos ajudar a trabalhar nessa coleção
         *  - - -
         *  filter - Vai receber um expressao lambda que vai poder fazer a a conta para gente
         *  Estamos passando uma expressao lambda que o resultado precisa ser booleano
         *  Ele vai verificar para cada fatura se e maior que 250
         *  Vai nos devolver um novo stream de faturas
         */

        faturas.stream()
                .filter(Fatura::estaEmrisco)//verifica se o retorno dessa função e true ou false
                .forEach(System.out::println);//da um print no resultado por que so tem 1 parametro f -> System.out.println(f)



        //st.forEach(f -> System.out.println(f));

        /**
         * Ele nao vai filtrar o faturas
         */
        //faturas.forEach(f -> System.out.println(f));




    }
}
