package sistem.algaworks.exemplos;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;
import sistem.algaworks.model.Carro;

import java.time.Year;
import java.util.Arrays;
import java.util.List;

public class CadastroCarro {

    public static void main(String[] args) {

        /**
         * Usando a classe year para instanciar um objeto do tipo carro
         * passando um inteiro
         */
        Carro gol = new Carro("Gol", 90.00, Year.of(2012));
        /**
         * Nesse caso ele aceita direto uma string
         */
        Carro cruze = new Carro("Cruze", 200.0, Year.parse("2018"));

        /**
         * Passando Inteiro
         */
        Carro celta = new Carro("Celta", 80.0, Year.of(2011));


        List<Carro> carros = Arrays.asList(gol, cruze, celta);

        /**
         * Utilizando os metodos
         * isAfter()
         * isBefore()
         * Com expressoes lambda
         * .isAfter(Year.of(2012) - queremos saber se os carros sao fabricados depos de 2012
         */
        carros.stream()
                .filter(carro -> carro.getAnoFabricacao().isAfter(Year.of(2012)))
                .forEach(carro -> System.out.println(carro.toString()));
    }

}
