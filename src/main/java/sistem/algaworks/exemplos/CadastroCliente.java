package sistem.algaworks.exemplos;

import sistem.algaworks.model.Cliente;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.util.Calendar;

public class CadastroCliente {
    public static void main(String[] args) {
        Cliente cliente = new Cliente("Joao Silva", LocalDate.of(2010, Month.AUGUST, 13));

        /**
         * Saber a diferenca entre a data de nascimento do cliente e hoje
         * getYears() pegar em anos
         * Novo Jeito do DateTime
         */
        int idade = Period.between(cliente.getDataNascimento(), LocalDate.now()).getYears();

        if(idade >= 18){
            System.out.println("ok Pode ser cadastrado, sua idade é "+ idade);
        }else{
            System.out.println("Sua idade é %d. Então não pode ter carteira nem cadastro no sistema."+ idade);
        }


        /**
         * Jeito antigo de fazer
         */
        /**Calendar idade = Calendar.getInstance();
        idade.set(Calendar.YEAR, 2004);

        Calendar hoje = Calendar.getInstance();

        long tempoEmMilesegundos = hoje.getTimeInMillis() - idade.getTimeInMillis();
        long anos = tempoEmMilesegundos / 1000 / 60 / 60 / 24 / 30 / 12;
        System.out.println(anos); **/
    }
}
