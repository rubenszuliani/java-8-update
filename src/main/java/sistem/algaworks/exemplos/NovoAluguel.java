package sistem.algaworks.exemplos;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import sistem.algaworks.model.Aluguel;
import sistem.algaworks.model.Carro;
import sistem.algaworks.model.Cliente;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.jar.JarOutputStream;

public class NovoAluguel {

    public static void main(String[] args) {

        Cliente cliente = new Cliente("Joao Silva", LocalDate.of(2010, Month.AUGUST, 13));

        Carro gol = new Carro("Gol", 90.00, Year.of(2012));

        /**
         * Trabalhando com LocaldateTime
         *
         * LocalTime.of(LocalTime.now().getHour(), 0) - Pegando a Hora fechada
         */
        LocalDateTime dataEHotaDaRetirada = LocalDateTime.of(LocalDate.now(), LocalTime.of(LocalTime.now().getHour(), 0));

        /**
         * Data e hora prevista para devolução
         * Vamos somar a data atual com a data que o cliente quer devolver
         * LocalDateTime.now().plusDays(3) - informamos que sera acrescentado 3 dias a data do cliente
         * Tem plusHours()
         * plusMinutes()
         * Varios PLus
         * Podemos colocar varios Plus juntos plusDays(3).plusHours(2)
         *
         * Temos Tambem o Minus para tirar
         */
        LocalDateTime dataPrevistaParaDevolucao = LocalDateTime.now().plusDays(3).plusHours(2);
        Aluguel aluguel = new Aluguel(cliente, gol, dataEHotaDaRetirada, dataPrevistaParaDevolucao );

        //TODO: imprimir o recibo

        System.out.println(">>>>>>>>>>>>>>>>>>RECIBO<<<<<<<<<<<<<<<<<<<<");
        System.out.println("Carro :" + aluguel.getCarro().getModelo());
        System.out.println("Cliente :" + aluguel.getCliente().getNome());

        //Formatando a Hora, para passar como queremos na função format
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        System.out.println("Data e Hora da Retirada :" + aluguel.getDataEHotaDaRetirada().format(formatter));
        System.out.println("Data e Hora Prevista Para devoução :" + aluguel.getDataPrevistaParaDevolucao().format(formatter));

    }

}
