package sistem.algaworks.model;

import java.time.LocalDateTime;

public class Aluguel {

    private Cliente cliente;
    private Carro carro;

    /**
     * LocalDateTime Representa Tanto a hora como dia mes e ano
     */
    private LocalDateTime dataEHotaDaRetirada;
    private LocalDateTime dataPrevistaParaDevolucao;

    public Aluguel(Cliente cliente, Carro carro, LocalDateTime dataEHotaDaRetirada, LocalDateTime dataPrevistaParaDevolucao) {
        this.cliente = cliente;
        this.carro = carro;
        this.dataEHotaDaRetirada = dataEHotaDaRetirada;
        this.dataPrevistaParaDevolucao = dataPrevistaParaDevolucao;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Carro getCarro() {
        return carro;
    }

    public void setCarro(Carro carro) {
        this.carro = carro;
    }

    public LocalDateTime getDataEHotaDaRetirada() {
        return dataEHotaDaRetirada;
    }

    public void setDataEHotaDaRetirada(LocalDateTime dataEHotaDaRetirada) {
        this.dataEHotaDaRetirada = dataEHotaDaRetirada;
    }

    public LocalDateTime getDataPrevistaParaDevolucao() {
        return dataPrevistaParaDevolucao;
    }

    public void setDataPrevistaParaDevolucao(LocalDateTime dataPrevistaParaDevolucao) {
        this.dataPrevistaParaDevolucao = dataPrevistaParaDevolucao;
    }
}
