package sistem.algaworks.model;

import java.time.Year;

public class Carro {

    private String modelo;
    private Double valorDiaria;
    private Year anoFabricacao; //yer e uma classe do java 8 para representar o ano

    public Carro(String modelo, Double valorDiaria, Year anoFabricacao) {
        this.modelo = modelo;
        this.valorDiaria = valorDiaria;
        this.anoFabricacao = anoFabricacao;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Double getValorDiaria() {
        return valorDiaria;
    }

    public void setValorDiaria(Double valorDiaria) {
        this.valorDiaria = valorDiaria;
    }

    public Year getAnoFabricacao() {
        return anoFabricacao;
    }

    public void setAnoFabricacao(Year anoFabricacao) {
        this.anoFabricacao = anoFabricacao;
    }

    @Override
    public String toString() {
        return "modelo " +modelo+ " Ano de Fabricacao " + anoFabricacao + " Valor da Diaria " + valorDiaria;
    }
}
