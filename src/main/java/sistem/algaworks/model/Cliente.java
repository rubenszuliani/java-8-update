package sistem.algaworks.model;

import java.time.LocalDate;

/**
 * Vamos criar um sistema para ver a nova api de data LocalDate
 * Tem apenas Ano/ Mes / Dia - não tem informação de hora
 */
public class Cliente {

    private String nome;
    private LocalDate dataNascimento;

    public Cliente(String nome, LocalDate dataNascimento) {
        this.nome = nome;
        this.dataNascimento = dataNascimento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
}

